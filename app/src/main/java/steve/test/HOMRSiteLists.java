package steve.test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.geotools.cs.GeodeticCalculator;
import org.geotools.factory.FactoryConfigurationError;
import org.geotools.feature.AttributeType;
import org.geotools.feature.AttributeTypeFactory;
import org.geotools.feature.Feature;
import org.geotools.feature.FeatureType;
import org.geotools.feature.FeatureTypeFactory;
import org.geotools.feature.IllegalAttributeException;
import org.geotools.feature.SchemaException;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;

import gov.noaa.ncdc.wct.decoders.DecodeException;
import gov.noaa.ncdc.wct.decoders.StreamingProcessException;
import gov.noaa.ncdc.wct.decoders.nexrad.DecodeL3Header;
import gov.noaa.ncdc.wct.decoders.nexrad.RadarHashtables;
import gov.noaa.ncdc.wct.export.vector.StreamingShapefileExport;

public class HOMRSiteLists {

	public final static boolean COMPARE_TO_L3_FILES = true;
	
	public static void main(String[] args) {
		
		try {

			processNEXRAD();


		} catch (IOException | FactoryConfigurationError | SchemaException | IllegalAttributeException | StreamingProcessException | DecodeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static void processNEXRAD() throws MalformedURLException, IOException, 
		FactoryConfigurationError, SchemaException, IllegalAttributeException, StreamingProcessException, DecodeException {
		
		

	    AttributeType[] nexradAttributeTypeArray = {
	        AttributeTypeFactory.newAttributeType("geom", Geometry.class),
	        AttributeTypeFactory.newAttributeType("icao", String.class),
	        AttributeTypeFactory.newAttributeType("latitude", Double.class),
	        AttributeTypeFactory.newAttributeType("longitude", Double.class),
	        AttributeTypeFactory.newAttributeType("elev", Integer.class),
	        AttributeTypeFactory.newAttributeType("name", String.class),
	        AttributeTypeFactory.newAttributeType("state", String.class),
	        AttributeTypeFactory.newAttributeType("country", String.class),
	        AttributeTypeFactory.newAttributeType("wban", String.class),
	        AttributeTypeFactory.newAttributeType("ncei_id", Integer.class),
	        AttributeTypeFactory.newAttributeType("county", String.class),
	        AttributeTypeFactory.newAttributeType("timezone", String.class)
	    };
		
		FeatureType nexradSitesSchema = FeatureTypeFactory.newFeatureType(nexradAttributeTypeArray, "Nexrad Site Attributes");
		GeometryFactory geoFactory = new GeometryFactory();
		int geoIndex = 0;
		GeodeticCalculator geocalc = new GeodeticCalculator();
		
		
		List<String> lines = IOUtils.readLines(new URL("https://www.ncdc.noaa.gov/homr/file/nexrad-stations.txt").openStream());
		List<String> l3Lines = new ArrayList<String>();
		l3Lines.add("icao,l3lat,l3lon,l3elev,lat,lon,elev,name,state,country,wban,ncei_id,county,timezone");

		File outFile = new File("C:\\work\\wct\\overlays\\wsr.shp");
		outFile.getParentFile().mkdirs();
		StreamingShapefileExport shpExport = new StreamingShapefileExport(outFile);
		for (String line : lines) {
//			System.out.println(line);
			if (line.startsWith("NCDCID") || line.startsWith("-")) {
				continue;
			}
			
//			0         1         2         3         4         5         6         7         8         9         0          1        2         3         4
//			012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
//			30001794 KABR 14929 ABERDEEN                       UNITED STATES        SD BROWN                          45.45583  -98.41306  1302   -6    NEXRAD     
//			NCDCID   ICAO WBAN  NAME                           COUNTRY              ST COUNTY                         LAT       LON        ELEV   UTC   STNTYPE 
			
			Integer nceiID = Integer.parseInt(line.substring(0, 9).trim());
			String icao = line.substring(9, 14).trim();
			String wban = line.substring(14, 20).trim();
			String name = line.substring(20, 51).trim();
			String country = line.substring(51, 72).trim();
			String state = line.substring(72, 75).trim();
			String county = line.substring(75, 106).trim();
			Double lat = Double.parseDouble(line.substring(106, 116).trim());
			Double lon = Double.parseDouble(line.substring(116, 127).trim());
			Integer elev = Integer.parseInt(line.substring(127, 134).trim());
			String timezone = line.substring(134, 140).trim();
			String stationType = line.substring(140).trim();


//	        AttributeTypeFactory.newAttributeType("geom", Geometry.class),
//	        AttributeTypeFactory.newAttributeType("icao", String.class),
//	        AttributeTypeFactory.newAttributeType("latitude", Double.class),
//	        AttributeTypeFactory.newAttributeType("longitude", Double.class),
//	        AttributeTypeFactory.newAttributeType("elev", Integer.class),
//	        AttributeTypeFactory.newAttributeType("name", String.class),
//	        AttributeTypeFactory.newAttributeType("state", String.class),
//	        AttributeTypeFactory.newAttributeType("country", String.class),
//	        AttributeTypeFactory.newAttributeType("wban", String.class),
//	        AttributeTypeFactory.newAttributeType("ncei_id", Integer.class),
//	        AttributeTypeFactory.newAttributeType("county", String.class),
//	        AttributeTypeFactory.newAttributeType("timezone", String.class)
			
			
            Coordinate coord = new Coordinate(lon, lat);
            Feature feature = nexradSitesSchema.create(new Object[] {
                    geoFactory.createPoint(coord),
                    icao, lat, lon, elev, name, state, country, wban, nceiID, county, timezone
            }, new Integer(geoIndex++).toString());

			shpExport.addFeature(feature);
	
			
			// compare to existing entry in wsr.dbf
			double tolerance = 0.0001; // degrees ~ 10m
			double oldLat = RadarHashtables.getSharedInstance().getLat(icao);
			double oldLon = RadarHashtables.getSharedInstance().getLon(icao);
			if (Math.abs(lat-oldLat) > tolerance || Math.abs(lon-oldLon) > tolerance) {
				System.err.println("Site location change! ::: "+icao+": old lat = "+oldLat+" current HOMR lat = "+lat);
				System.err.println("Site location change! ::: "+icao+": old lon = "+oldLon+" current HOMR lon = "+lon);
			}
			
			if (COMPARE_TO_L3_FILES) {
				try {
					double l3tolerance = 0.001;
					// DPA product exists for both NEXRAD and TDWR
					// Example URL: https://tgftp.nws.noaa.gov/SL.us008001/DF.of/DC.radar/DS.81dpr/SI.kiwx/sn.last
					URL url = new URL("https://tgftp.nws.noaa.gov/SL.us008001/DF.of/DC.radar/DS.81dpr/SI."+icao.toLowerCase()+"/sn.last");
					System.out.println("Checking HOMR "+icao+" to: "+url);
					Thread.sleep(1000L);
					DecodeL3Header l3header = new DecodeL3Header();
					l3header.decodeHeader(url);
					

//					l3Lines.add("icao,l3lat,l3lon,l3elev,lat,lon,elev,name,state,country,wban,ncei_id,county,timezone");
					l3Lines.add(icao+","+l3header.getLat()+","+l3header.getLon()+","+l3header.getAlt()+
							","+lat+","+lon+","+elev+","+name+","+state+","+country+","+wban+","+nceiID+","+county+","+timezone);

					// compare homr to lat/lon in Level-3 file header in latest file available from NWS
					if (Math.abs(l3header.getLat()-lat) > l3tolerance || l3header.getLon()-lon > l3tolerance) {
						System.err.println("HOMR-to-L3files mismatch! ::: "+icao+": L3 file lat = "+l3header.getLat()+" current HOMR lat = "+lat);
						System.err.println("HOMR-to-L3files mismatch! ::: "+icao+": L3 file lon = "+l3header.getLon()+" current HOMR lon = "+lon);
						
						geocalc.setAnchorPoint(lon, lat);
		            	geocalc.setDestinationPoint(l3header.getLon(), l3header.getLat());
		            	System.err.println(icao+": distance between HOMR list and L3 File Header locations: "+geocalc.getOrthodromicDistance()+" meters.");
					}
				} catch (Exception e) {
					System.err.println(e.getMessage());
				}
			}
			
		}
		shpExport.close();		
		System.out.println("HOMR station list written to :"+outFile);
		System.out.println("This file may be copied to <workspace>/wct/ext/shapefiles");
		
		if (COMPARE_TO_L3_FILES) {
			IOUtils.writeLines(l3Lines, null, new FileWriter(outFile+".csv"));
		}
	}
}
