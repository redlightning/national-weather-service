package gov.noaa.ncdc.wct.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import org.geotools.feature.Feature;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureCollections;
import org.geotools.feature.FeatureIterator;
import org.geotools.gui.swing.tables.FeatureTableModel;
import org.geotools.map.DefaultMapLayer;
import org.geotools.map.MapContext;
import org.geotools.map.MapLayer;
import org.geotools.renderer.j2d.LegendPosition;
import org.geotools.renderer.j2d.RenderedLogo;
import org.geotools.styling.Graphic;
import org.geotools.styling.Mark;
import org.geotools.styling.PointSymbolizer;
import org.geotools.styling.Style;
import org.geotools.styling.StyleBuilder;
import org.geotools.styling.Symbolizer;
import org.geotools.styling.TextSymbolizer;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXTable;

import com.vividsolutions.jts.geom.Geometry;

import gov.noaa.ncdc.common.RiverLayout;
import gov.noaa.ncdc.wct.WCTProperties;
import gov.noaa.ncdc.wct.WCTUtils;
import gov.noaa.ncdc.wct.decoders.nexrad.RadarHashtables;
import gov.noaa.ncdc.wct.decoders.nexrad.RadarHashtables.SearchFilter;
import gov.noaa.ncdc.wct.io.ScanResults;
import gov.noaa.ncdc.wct.io.WCTDataSourceDB;

public class GridLegendEditorUI extends JDialog {
	
	private SpcStormReports stormReports = null;
	private MapLayer hailReportsMapLayer, tornadoReportsMapLayer, windReportsMapLayer;
	private ArrayList<Feature> mappedFeatures = new ArrayList<Feature>();
	private FeatureCollection selectedFeatures = FeatureCollections.newCollection();
	private MapLayer selectedReportsMapLayer = null;
	private final JXDatePicker picker = new JXDatePicker(new Date(), Locale.US);
	private final FeatureTableModel featureTableModel = new FeatureTableModel();
	private final ArrayList<String> narrativeList = new ArrayList<String>();
	private final JTextPane narrativeTextPane = new JTextPane();
    private final JXTable reportsTable = new JXTable();
	
	public final static SimpleDateFormat SDF_YYYYMMDD = new SimpleDateFormat("yyyyMMdd");
	
	private WCTViewer viewer;
	private JCheckBox clearOnClose;
	private JButton displayButton, clearButton;
	private JLabel statusLabel = new JLabel();
	
	
    
    public GridLegendEditorUI(WCTViewer parent) {      
        super(parent, "Legend Editor", false);
        this.viewer = parent;
        
        init();        
        
        createGUI();
        pack();
        
    }

    private void init() {
    	try {
//			stormReports = new SpcStormReports();
		} catch (Exception e) {
			e.printStackTrace();
            javax.swing.JOptionPane.showMessageDialog(this, "Init Exception: "+e, 
                    "INIT EXCEPTION", javax.swing.JOptionPane.ERROR_MESSAGE);

		}
    }


    private void createGUI() {

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new RiverLayout());

        final JDialog finalThis = this;

        final JTextField jtfTitleLine1 = new JTextField("$filename");
        final JTextField jtfTitleLine2 = new JTextField("$var");
        mainPanel.add(jtfTitleLine1, "hfill br");
        mainPanel.add(jtfTitleLine2, "hfill br");
        


        this.add(mainPanel);
        this.pack();
//        this.setSize(700, 600);

                
        
        
        JRootPane rootPane = this.getRootPane();
        InputMap iMap = rootPane.getInputMap( JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        iMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "escape");

        ActionMap aMap = rootPane.getActionMap();
        aMap.put("escape", new AbstractAction()
        {
            public void actionPerformed(ActionEvent e)
            {
                dispose();
            }
        });
        
        
        
        
        

    }
    
    
    
    
}
