package gov.noaa.ncdc.wct.ui;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import gov.noaa.ncdc.common.RiverLayout;
import gov.noaa.ncdc.nexradiv.legend.CategoryLegendImageProducer;
import gov.noaa.ncdc.wct.WCTException;


public class LegendEditor extends JDialog {

    private WCTViewer viewer;
    
    private static LegendEditor singleton = null;
    
        
    private boolean engageCustomLegendOverride = false;
    
    
    public static LegendEditor getInstance(final Frame parent, final WCTViewer viewer) {
    	if (singleton == null) {    		
    		singleton = new LegendEditor(parent, viewer);
    	}
    	return singleton;
    }
    
    
    private LegendEditor(Frame parent, WCTViewer viewer) {      
        super(parent, "Legend Editor", false);
        this.viewer = viewer;
        createGUI();
        pack();
    }
    
    
    
    private void createGUI() {
        
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new RiverLayout());
        
        final JTextField jtfTitle = new JTextField(50);
        jtfTitle.addKeyListener(new KeyAdapter() {
			
			@Override
			public void keyReleased(KeyEvent evt) {
				try {
//					String origTitleString = jtfTitle.getText() + evt.getKeyChar();
					String origTitleString = jtfTitle.getText();
					String titleString = replaceSpecialKeywords(origTitleString);
					CategoryLegendImageProducer legendProducer = viewer.getGridSatelliteLegendImageProducer();
					legendProducer.setLegendTitle(new String[] { titleString });
					viewer.getGridSatelliteLegend().setImage(legendProducer.createMediumLegendImage());
					viewer.getGridSatelliteLegend().repaint();
					viewer.getMapPane().repaint();
				} catch (WCTException e) {
					e.printStackTrace();
				}
			}			
		});
        
        
        final JCheckBox jcbEngage = new JCheckBox("Engage Continued Legend Override");
        jcbEngage.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent evt) {
				engageCustomLegendOverride = jcbEngage.isSelected();
				
				System.out.println(viewer.getFileScanner().getLastScanResult());
			}
		});
        
        JComboBox dateTimeFormats = new JComboBox(new String[] {
        		"Year", "Year-Month", "Year-Month-Day", "Year-Month-Day  Hour", "Year-Month-Day  Hour:Minute", "Year-Month-Day  Hour:Minute:Second"
        });
        
        mainPanel.add(new JLabel("Legend Title: "));
        mainPanel.add(jtfTitle, "hfill br");
        mainPanel.add(new JCheckBox("Show Date/Time: "));
        mainPanel.add(new JLabel("Date/Time Formats: "));
        mainPanel.add(dateTimeFormats);
        mainPanel.add(jtfTitle, "hfill br");
        mainPanel.add(new JLabel("Font: "));
        mainPanel.add(new JButton("Font Chooser"));
        mainPanel.add(jcbEngage, "br");
        
        
        this.getContentPane().add(mainPanel);
        
    }

    private String replaceSpecialKeywords(String str) {

    	if (str.matches(".*\\$substring-filename\\(.*\\).*")) {
    		try {
    		String indices = str.substring(str.indexOf("(")+1, str.indexOf(")"));
    		int startIndex = Integer.parseInt(indices.split(",")[0].trim());
    		int endIndex = Integer.parseInt(indices.split(",")[1].trim());
    		String filename = viewer.getFileScanner().getLastScanResult().getFileName();
    		System.out.println(str+": "+startIndex+" " +endIndex+" " +filename.substring(startIndex, endIndex));
    		str = str.replaceAll("\\$substring-filename\\(.*\\)", filename.substring(startIndex, endIndex));
    		} catch (Exception e) {
    			System.out.println(e.toString());
    		}
    	}
    	if (str.contains("$filename")) {
    		str = str.replace("$filename", viewer.getFileScanner().getLastScanResult().getFileName());
    	}
    	if (str.contains("$displayname")) {
    		str = str.replace("$displayname", viewer.getFileScanner().getLastScanResult().getDisplayName());
    	}
    	if (str.contains("$longname")) {
    		str = str.replace("$longname", viewer.getFileScanner().getLastScanResult().getLongName());
    	}
    	if (str.contains("$variable")) {
    		try {
				str = str.replace("$variable", viewer.getGridDatasetRaster().getVariableName());
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	if (str.contains("$units")) {
    		try {
				str = str.replace("$units", viewer.getGridDatasetRaster().getUnits());
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	return str;
    }

	public boolean isEngageCustomLegendOverride() {
		return engageCustomLegendOverride;
	}


	public void setEngageCustomLegendOverride(boolean engageCustomLegendOverride) {
		this.engageCustomLegendOverride = engageCustomLegendOverride;
	}
}
